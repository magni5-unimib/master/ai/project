'''
MyAgent class.
'''


#region imports

# basic
import functools

# langchain
from langchain.tools import BaseTool
from langchain_core.language_models.base import BaseLanguageModel
from langchain import hub
from langchain.prompts import PromptTemplate
from langgraph.graph import StateGraph
from langgraph.pregel import Pregel

# mine
import lg

#endregion


class MyAgent():
    llm: BaseLanguageModel = None
    agent_workflow: StateGraph = None
    agent_graph: Pregel = None

    tools: [BaseTool] = []
    name: str = 'MyAgent'
    system_prompt: str = 'You are a custom agent.'
    start_message: str = 'Tell me a joke about start messages.'

    def __init__(
        self,
        llm: BaseLanguageModel,
        agent_workflow: StateGraph = None,
        *args,
        **kwargs
    ):
        self.llm = llm
        self.setup_agent()
        self.setup_tools()
        if agent_workflow is not None:
            self.agent_workflow = agent_workflow
            self.add_to_workflow()

    def setup_agent(self):
        pass

    def setup_tools(self):
        pass

    def add_to_workflow(self):
        traveler_agent = lg.create_agent(
            self.llm,
            self.tools,
            self.system_prompt
        )
        lg.create_and_add_node(traveler_agent, self.name, self.agent_workflow)

    def invoke(self, start_message: str):
        '''
        Invoke the agent graph and return the final response.
        It also compile the workflow if it is the first time it's invoked.
        It is not compiled at initialization time because the workflow could
        still be under construction, but if it's invoked then it is assumed that
        the workflow it's ready.
        '''
        if self.agent_graph is None:
            self.agent_graph = self.agent_workflow.compile()

        final_response = lg.invoke(self.agent_graph, start_message)
        print(f'\n\nthe final response of the llm is:\n{final_response}\n\n')
        return final_response
