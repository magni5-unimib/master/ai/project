'''
My specific graph definition
'''


from networkx import Graph


class MyGraph(Graph):
    '''
    The same behaviour as networkx's Graph class, but extended with methods
    to retrieve coordinates according to my specifically designed grid.
    '''

    def v_x(self, vertex_id: int) -> int:
        '''
        Returns the x coordinate of the given vertex.
        '''
        return (vertex_id // 10) * 2

    def v_y(self, vertex_id: int) -> int:
        '''
        Returns the y coordinate of the given vertex.
        '''
        return (vertex_id % 10) * 2

    def e_x(self, source_id: int, target_id: int) -> int:
        '''
        Returns the x coordinate of the edge connecting the given vertices.
        '''
        return (self.v_x(source_id) + self.v_x(target_id)) / 2

    def e_y(self, source_id: int, target_id: int) -> int:
        '''
        Returns the y coordinate of the edge connecting the given vertices.
        '''
        return (self.v_y(source_id) + self.v_y(target_id)) / 2


V = []
for i in range(100):
    V.append(i)

E = []
for v in V:
    if v >= 10:
        # edge with node above
        E.append((v, v - 10, { 'weight': 1}))
    if v < 90:
        # edge with node below
        E.append((v, v + 10, { 'weight': 1}))
    if v % 10 > 0:
        # edge with node to the left
        E.append((v, v - 1, { 'weight': 1}))
    if v % 10 < 9:
        # edge with node to the right
        E.append((v, v + 1, { 'weight': 1}))


def load() -> MyGraph:
    '''
    Returns an instance of my specifically designed graph
    '''
    graph = MyGraph()
    graph.add_nodes_from(V)
    graph.add_edges_from(E)
    return graph
