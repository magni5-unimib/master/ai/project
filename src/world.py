'''
This module contains all of the globals variables that are common to all classes
and objects, since their informations are related to the context "world" in
which the agents are assumed to operate.
'''


'''
The 'roads' and 'intersections' world in which the agents will move is
represented as a graph.
'''
from graph import MyGraph
G: MyGraph = None


'''
The object to interact with the NetLogo platform. Common to all the objects.
'''
from netlogo import MyNetLogoLink
NETLOGO: MyNetLogoLink


'''
The array of travelers 'alive' in the simulation.
Placing it here to let LangChain tools to access it if needed without
instanciating a new instance of Traveler based on the "who".
'''
from travelers import Traveler
TRAVELERS: [Traveler] = []

def get_traveler(who: int) -> Traveler:
    for t in TRAVELERS:
        if t.who == who:
            return t
    return None


'''
The SatNav. Not sure if this object should be here, actually.
'''
from satnav import SatNav
SATNAV = None


'''
The large language model that will be used by agents and tools.
'''
# The type of the LLM can be different
LLM = None


'''
The agents graph
'''
from langgraph.graph import StateGraph
AGENT_WORKFLOW: StateGraph = None
from langgraph.pregel import Pregel
AGENT_GRAPH: Pregel = None
