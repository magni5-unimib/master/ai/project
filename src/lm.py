'''
Provides configuration for the LLM and LangChain
'''


#region imports

# basic
import os
from dotenv import load_dotenv
load_dotenv()

# langchain
from langchain_openai import ChatOpenAI
from langchain_community.llms import Ollama

#endregion


#region config

LANGCHAIN_TRACING_V2 = 'true'
LANGCHAIN_ENDPOINT = 'https://api.smith.langchain.com'
LANGCHAIN_API_KEY = os.getenv('LANGCHAIN_API_KEY')
LANGCHAIN_PROJECT = 'playground'

OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')

MODEL_TO_USE = 'gpt-3.5'

#endregion


os.environ['LANGCHAIN_TRACING_V2'] = LANGCHAIN_TRACING_V2
os.environ['LANGCHAIN_API_KEY'] = os.getenv('LANGCHAIN_API_KEY')
os.environ['LANGCHAIN_PROJECT'] = LANGCHAIN_PROJECT


MODELS = {
    'tinyllama': { 
        'name': 'tinyllama',
        'embedding_size': 2048,
        'type': 'ollama' 
    },
    'phi': { 
        'name': 'phi',
        'embedding_size': 2560,
        'type': 'ollama' 
    },
    'llama2': { 
        'name': 'llama2',
        'embedding_size': 2048, # ?,
        'type': 'ollama' 
    },
    'mistral': { 
        'name': 'mistral',
        'embedding_size': 2048, # ?,
        'type': 'ollama' 
    },
    'gpt-3.5': { 
        'name': 'gpt-4',
        'embedding_size': 2048, # ?
        'type': 'openai'
    }
}

# to use openai interface but running ollama model (no function calling capabilities)
# LLM = ChatOpenAI(
#     base_url = 'http://localhost:11434/v1',
#     api_key = 'ollama', # required, but unused
#     model = 'llama2',
#     streaming = True
# )


def load():
    model = MODELS.get(MODEL_TO_USE)

    llm = None
    if model.get('type') == 'openai':
        llm = ChatOpenAI(
            model = model.get('name'),
            streaming = True,
            api_key = OPENAI_API_KEY
        )
    else:
        llm = Ollama(model = model.get('name'))

    return llm
