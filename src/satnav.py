'''
SatNav class
'''


#region imports

# networkx
import networkx as nx

# my
from graph import MyGraph

#endregion


#region classes

class SatNav():
    G: MyGraph = None
    shortest_paths: dict = None

    def set_graph(self, G: MyGraph):
        self.G = G
        self.process_shortest_paths()

    def process_shortest_paths(self):
        self.shortest_paths = dict(nx.all_pairs_shortest_path(self.G))

    def get_shortest_path(self, source: int, target: int) -> [int]:
        return self.shortest_paths[source][target]

    def next_step(self) -> int:
        pass

#endregion


#region tools

import json
from langchain_core.tools import tool
import world


@tool('CalculatePath', return_direct = False)
def calculate_path(input: str) -> str:
    '''
    This tool is useful to calculate the path between the source and the target
    given as input, for the Traveler identified by "who".
    
    All the parameters have to be passed as input in a json format.
    This is an input example: { "who": "3", "source": "5", "target": "25" }
    '''
    try:
        parameters = json.loads(input)
        shortest_path = nx.shortest_path(
            world.G,
            int(float(parameters['source'])), 
            int(float(parameters['target']))
        )
        t = world.get_traveler(int(parameters['who']))
        t.path = shortest_path
        return shortest_path
    except Exception as ex:
        return f'Something went wrong: {ex}.'

#endregion