'''
Travelers classes
'''


#region future

from __future__ import annotations

#endregion


#region imports

# langchain
from langgraph.graph import StateGraph
from langchain.llms import BaseLLM

# mine
from netlogo import Turtle
from satnav import SatNav
from agents import MyAgent

#endregion


#region config

RIDER_PASSENGER_ENABLED = True

#endregion


#region classes

class Traveler(Turtle):
    '''
    This class represent a traveler turtle
    '''

    source: int = -1
    target: int = -1

    satnav: SatNav = None
    path: [int] = None

    # I save this value to avoid re-calculating next patch if the traveler can't
    # move because it is occupied
    next_patch_xy: (int, int) = None

    def __init__(self, satnav: SatNav, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.breed = 'travelers'
        self.breed_unit = 'traveler'
        self.satnav = satnav
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.breed = 'travelers'
    #     self.breed_unit = 'traveler'

    def tick(self):
        '''
        The function to be performed at every tick of the NetLogo environment
        '''
        if self.get_attribute('[ vertex-id ] of patch-here') == self.target:
            self.end()
        elif self.path is None:
            self.calculate_path()
            self.print(f'calculated path is {self.path}')
        else:
            self.operate()

    def end(self):
        self.print('target!')
        self.die()

    def operate(self):
        if type(self) == TravelerAgent:
            print('traveler agent operating')

        if self.is_passenger():
            self.print(f'''chilling as passenger of {self.get_attribute('passenger-of')}''')
        elif self.next_patch_xy is not None:
            self.print('i know the next patch, let\'s move')
            self.move()
        else:
            self.print('planning next move')
            self.plan_next_move()

    def plan_next_move(self):
        '''
        In this function the traveler understand where is it, and decide which
        is the next move to do
        '''
        on_vertex, current_vertex = self.on_vertex()
        on_edge, current_edge_source, current_edge_target = self.on_edge()

        if not (on_vertex or on_edge):
            self.print('not on vertex or edge patch')
            self.die()
        else:
            i = 0
            found = False
            while i < len(self.path) and not found:
                if (on_vertex and self.path[i] == current_vertex) \
                        or (on_edge \
                            and (self.path[i] == current_edge_source \
                                or self.path[i] == current_edge_target)):
                    found = True
                else:
                    i += 1

            if not found:
                self.print('lost')
                self.die()
            else:
                next_vertex = self.path[i + 1]
                if on_vertex:
                    # leave this if it is needed to change and save the info of
                    # the next patch in netlogo attributes
                    # self.set_attribute(
                    #     'next-patch',
                    #     f'''patch
                    #             {self.satnav.G.e_x(current_vertex, next_vertex)}
                    #             {self.satnav.G.e_y(current_vertex, next_vertex)}'''
                    # )
                    self.next_patch_xy = (
                        self.satnav.G.e_x(current_vertex, next_vertex),
                        self.satnav.G.e_y(current_vertex, next_vertex)
                    )
                else: # on_edge
                    self.next_patch_xy = (
                        self.satnav.G.v_x(next_vertex),
                        self.satnav.G.v_y(next_vertex)
                    )

    def move(self):
        '''
        This function make the traveler face the destination path.
        If it is free it moves to it, eventually bringing its passenger.
        If it is occupied, it tries to get a rider from the occupier.
        Otherwise, it does nothing.
        '''
        x, y = self.next_patch_xy
        self.ask(f'face patch {x} {y}')
        # had to introduce this condition because travelers don't die when they
        # reach the target
        if self.get_attribute(f'[ vertex-id ] of patch {x} {y}') == self.target:
            free = True
        else:
            free = self.netlogo.report(
                f'not any? {self.breed}-on patch {x} {y}'
            )

        if free:
            self.ask(f'''
                move-to patch {x} {y}
                set distance-covered distance-covered + 1
            ''')
            self.next_patch_xy = None

            if self.has_passenger():
                passenger = Traveler(
                    self.satnav,
                    self.netlogo,
                    self.get_attribute('has-passenger')
                )
                passenger.ask(f'''
                    ; ask {self.breed_unit} {passenger} [
                        move-to {self.breed_unit} {self.who}
                        set distance-saved distance-saved + 1
                    ;]
                ''')
        elif RIDER_PASSENGER_ENABLED and not self.has_passenger():
            traveler_on_next_patch = self.get_traveler_on_patch(x, y)
            rider = Traveler(self.satnav, self.netlogo, traveler_on_next_patch)
            ride_accepted = self.ask_a_ride(rider)
            if not ride_accepted:
                self.wait()
        else:
            self.wait()

    def wait(self):
        self.print('waiting')
        self.ask('set waiting-time waiting-time + 1')

    def on_vertex(self) -> (bool, int):
        '''
        Is the traveler on a vertex patch? If true returns the vertex-id, too
        '''
        return (
            self.get_attribute('member? patch-here vertices'),
            self.get_attribute('[ vertex-id ] of patch-here')
        )

    def on_edge(self) -> (bool, int, int):
        '''
        Is the traveler on a edge patch? If true returns the edge-source and
        edge-target, too
        '''
        return (
            self.get_attribute('member? patch-here edges'),
            self.get_attribute('[ edge-source ] of patch-here'),
            self.get_attribute('[ edge-target ] of patch-here'),
        )

    def calculate_path(self):
        '''
        Function that makes use of the SatNav object to calculate the path to
        follow in order to reach the target vertex
        '''
        self.path = self.satnav.get_shortest_path(self.source, self.target)

    def get_traveler_on_patch(self, x: int, y: int):
        return self.netlogo.report(
            f'[ who ] of [ one-of {self.breed}-here ] of patch {x} {y}'
        )

    def ask_a_ride(self, rider: Traveler):
        if rider.can_give_a_ride():
            rider.give_a_ride(self)
            return True
        return False

    def can_give_a_ride(self):
        return not (self.is_passenger() or self.has_passenger())

    def give_a_ride(self, passenger: Traveler):
        self.set_attribute('has-passenger', passenger.who)
        self.set_attribute('color', 'violet')
        passenger.get_a_ride(self)

    def get_a_ride(self, rider: Traveler):
        self.set_attribute('passenger-of', rider.who)
        # self.set_attribute('color', 'cyan')
        self.set_attribute('hidden?', 'true')
        self.ask(f'move-to {rider.get_turtle()}')

    def is_passenger(self):
        '''
        Check if the traveler is riding a passenger.
        The functions help in not repeating every time to check if the attribute
        value is one of the following that could be returned by NetLogo.
        '''
        return self.get_attribute('passenger-of') not in self.null_values

    def has_passenger(self):
        '''
        Check if the traveler is passenger of another.
        The functions help in not repeating every time to check if the attribute
        value is one of the following that could be returned by NetLogo.
        '''
        return self.get_attribute('has-passenger') not in self.null_values


class TravelerAgent(Traveler, MyAgent):

    name: str = 'Traveler_{self.who}'
    system_prompt: str = '''
        You are the traveler identified by "who" equal to {self.who}.
        You have to reach your target vertex.
        Remember that you need to pass your identifying "who" to every tool you
        use.
        Every time you use the PathInfo tool you have to report back the
        result to the Supervisor, otherwise you will go to prison.
    '''
    start_message: str = '''
        Help the traveler {self.who} to pursue his goal.

        An example of a correct sequence of actions is:
        - ask the Traveler about PathInfo
        - report to Supervisor
        - ask the SatNavManager to CalculatePath
        - report to Supervisor
        - ask the Traveler to Move
        - report to Supervisor
        - FINISH
    '''

    do_operate: bool = False

    def __init__(
        self,
        llm: BaseLanguageModel,
        agent_workflow: StateGraph = None,
        *args,
        **kwargs
    ):
        Traveler.__init__(self, *args, **kwargs)
        MyAgent.__init__(self, llm, agent_workflow, *args, **kwargs)

    def setup_agent(self):
        self.name = self.name.replace('{self.who}', str(self.who))
        self.system_prompt = self.system_prompt.replace(
            '{self.who}', 
            str(self.who)
        )
        self.start_message = self.start_message.replace(
            '{self.who}', 
            str(self.who)
        )

    def setup_tools(self):
        self.tools += [
            path_info,
            # save_path,
            move,
            # park
        ]

    def tick(self):
        self.print(f'reasoning...')
        result = self.invoke(self.start_message)
        self.print(result)
        if self.do_operate:
            self.operate()
        self.do_operate = False

#endregion


#region tools

import json
from langchain_core.tools import tool
import world


def parse_input(input: str) -> dict:
    try:
        parameters = json.loads(input) # input is a valid json
    except Exception as e1:
        try:
            parameters = { 'who': f'{int(input)}' } # i.e. input = 5
        except Exception as e2:
            raise ValueError('Not valid input for the tool.') # input not json
    else:
        if type(parameters) is int: # i.e. input = '5'
            parameters = { 'who': f'{parameters}' }
    return parameters


@tool('Park', return_direct = False)
def park(input: str) -> str:
    '''
    This tool allows the traveler (identified by "who") to perform the final
    operations when he has reached the target vertex.

    All the parameters have to be passed as input in a json format.
    This is an input example to end the trip of traveler 5: { "who": "5" }
    '''
    try:
        parameters = parse_input(input)
        who = int(parameters['who'])
        t = world.get_traveler(who)
        t.alive = False #.end()
        return 'My trip is ended. There\'s nothing more to do here.'
    except Exception as ex:
        return f'Something went wrong: {ex}'


@tool('PathInfo', return_direct = False)
def path_info(input: str) -> str:
    '''
    This tool allows the traveler (identified by "who") to know if he has
    already calculated and saved his path.
    If the answer is negative it provides the source and target vertices.

    All the parameters have to be passed as input in a json format.
    This is an input example to know if traveler 5 has already calculated his
    path: { "who": "5" }
    '''
    try:
        parameters = parse_input(input)
        who = int(parameters['who'])
        t = world.get_traveler(who)
        if t.path is None:
            return f'''
                I don't know my path.
                My source is "{t.source}" and my target is
                "{t.target}".
                The next action to take is to go back to the Supervisor.
            '''
        return 'I already know and saved my path.'
    except Exception as ex:
        return f'Something went wrong: {ex}.'


@tool('SavePath', return_direct = False)
def save_path(input: str) -> str:
    '''
    This tool allows the traveler (identified by "who") to save the path
    information.
    This is not a path calculator.

    All the parameters have to be passed as input in a json format.
    This is an input example to save the path of traveler 5:
        { "who": "5", "path": [ 3, 7, 9, 4, 2] }
    '''
    try:
        parameters = parse_input(input)
        who = int(parameters['who'])
        t = world.get_traveler(who)
        t.path = parameters['path']
        if t.path[0] != t.source \
                or t.path[len(t.path) - 1] != t.target:
            t.path = None
            return '''
                The path doesn't exists. It's better to calculate it again.
                The source is "{t.source}" and the target is "{t.target}".
            '''
        return 'Path saved.'
    except Exception as ex:
        return f'Something went wrong: {ex}.'


@tool('AskARide', return_direct = False)
def ask_a_ride(input: str) -> str:
    '''
    This tool allows the traveler (identified by "who") to ask some other
    travelers for a ride.

    All the parameters have to be passed as input in a json format.
    This is an input example to know if traveler 5 has already calculated his
    path: { "who": "5" }
    '''
    try:
        parameters = parse_input(input)
        who = int(parameters['who'])
        t = world.get_traveler(who)
        #
        return ''
    except Exception as ex:
        return f'Something went wrong: {ex}.'


@tool('Move', return_direct = False)
def move(input: str) -> str:
    '''
    This tool allows the traveler (identified by "who") to move in his
    environment. After the traveler has moved he has finished his tasks.

    All the parameters have to be passed as input in a json format.
    This is an input example to allow the traveler 5 to operate: { "who": "5" }
    '''
    try:
        parameters = parse_input(input)
        who = int(parameters['who'])
        t = world.get_traveler(who)
        # t.operate() # not working in any way because of pyNetLogo
        if t.path is None:
            return '''
                The traveler is not moving beacause he doesn't know his path.
                He needs to know my path in order to move correctly.
            '''
        t.do_operate = True
        return '''The Traveler moved, now it's time to end the conversation.'''
    # except ValueError as ex: # this is needed in case of t.operate()
    #     return f'Something went wrong: {ex}.'
    except Exception as ex:
        print(f''' \n\n\n {ex} \n\n\n ''')
        return '''
            Something went wrong. It's better to end my trip and return to
            the supervisor.
        '''

#endregion
