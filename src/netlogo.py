'''
Netlogo related stuff
'''


#region imports

import os

from pynetlogo import NetLogoLink

#endregion


#region config

PLATFORMS = {
    'linux': { 
        'netlogo_home': '',
    },
    'macos': {
        'jvm_path': '/Library/Java/JavaVirtualMachines/jdk-20.jdk/Contents/MacOS/libjli.dylib',
        'jvm_home': '/Library/Java/JavaVirtualMachines/jdk-20.jdk/Contents/Home/lib/server/libjvm.dylib',
        'netlogo_home': '/Applications/NetLogo 6.4.0',
    },
    'windows': { },
}
PLATFORM = PLATFORMS.get('windows') # use this to set the platform
MODEL_PATH = './models/my_model.nlogo'
GUI = True # not working for macos
RANDOM_SEED = 5

#endregion


#region classes

class MyNetLogoLink(NetLogoLink):
    '''
    Inheriting pyNetLogo's NetLogoLink to extend it with custom functionalities.
    '''

    def output(
        self,
        what: str,
        empty_lines_before: int = 0,
        empty_lines_after: int = 0
    ):
        for _ in range(empty_lines_before):
            self.command('output-show ""')
        self.command(f'output-show "{what.strip()}"')
        for _ in range(empty_lines_after):
            self.command('output-show ""')


class Turtle():
    '''
    The class representing a turtle. It's useful to handle the interaction
    between a Python object and a turtle in the NetLogo environment.
    '''

    netlogo: NetLogoLink
    breed: str
    breed_unit: str
    who: int
    alive: bool
    null_values = [ None, -1, -1.0, '-1', '-1.0', '-1,0' ]

    def __init__(self, netlogo: NetLogoLink, who: int, *args, **kwargs):
        self.netlogo = netlogo
        self.who = who
        self.alive = True

    def whoami(self) -> str:
        return f'{self.breed_unit} {str(self.who)} ({self.__class__.__name__})'

    def get_turtle(self) -> str:
        return f'{self.breed_unit} {self.who}'

    def ask(self, what: str):
        self.netlogo.command(f'ask {self.get_turtle()} [ {what} ]')

    def get_attribute(self, key: str):
        return self.netlogo.report(f'[ {key} ] of {self.get_turtle()}')

    def set_attribute(self, key: str, value: str):
        self.ask(f'set {key} {value}')

    def die(self):
        self.print('die')
        # don't ask it to really die, otherwise its metrics will be lost
        # self.ask('die')
        self.ask('set hidden? true')
        self.alive = False

    def print(self, what: str):
        print(f'{self.whoami()}: {what}')
        what = what.rstrip()
        what = what.replace('"', '')
        what = what.replace('  ', ' ')
        self.ask(f'output-show "{what}"')

    def tick(self):
        '''
        The tick command. Every child specify what to do actually by overriding
        this method.
        '''

#endregion


#region functions

def load() -> MyNetLogoLink:
    netlogo = MyNetLogoLink(
        gui = GUI,
        jvm_path = PLATFORM.get('jvm_path'),
        netlogo_home = PLATFORM.get('netlogo_home')
    )
    modelfile = os.path.abspath(MODEL_PATH)
    netlogo.load_model(modelfile)
    # set a random-seed to allow reproducibility
    netlogo.command(f'random-seed {RANDOM_SEED}')
    netlogo.command('setup')
    return netlogo

#endregion


#region tools

import json
from langchain_core.tools import tool
import world


@tool('NetLogoReader', return_direct = False)
def get_netlogo_attribute(input: str) -> str:
    '''
    Gets the value of an attribute of a certain traveler (identified by "who").
    
    All the parameters have to be passed as input in a json format.
    This is an input example to get the value of the attribute name for the
    traveler 5: { "who": "5", "attribute": "name" }
    '''
    parameters = json.loads(input)
    t = world.get_traveler(int(parameters['who']))
    return t.get_attribute(parameters['attribute'])


@tool('NetLogoWriter', return_direct = False)
def set_netlogo_attribute(input: str) -> str:
    '''
    Sets the value of an attribute of a certain traveler (identified by "who").
    
    All the parameters have to be passed as input in a json format.
    This is an input example to set "Marco" as the value of the attribute name
    for the traveler 5: { "who": "5", "attribute": "name", "value": "Marco" }
    '''
    parameters = json.loads(input)
    t = world.get_traveler(int(parameters['who']))
    return t.set_attribute(parameters['attribute'], parameters['value'])

#endregion
