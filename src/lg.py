'''
TBD
'''

#region config

#endregion


#region imports

# basic
import operator
from typing import Annotated, Sequence, TypedDict
import functools

# langchain
from langchain.agents import AgentExecutor, create_openai_tools_agent
from langchain_core.messages import BaseMessage, HumanMessage
from langchain_openai import ChatOpenAI
from langchain.output_parsers.openai_functions import JsonOutputFunctionsParser
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langgraph.graph import StateGraph, END
from langgraph.pregel import Pregel

#endregion


#region classes

class AgentState(TypedDict):
    '''
    The agent state is the input to each node in the graph.
    '''
    # The annotation tells the graph that new messages will always
    # be added to the current states
    messages: Annotated[Sequence[BaseMessage], operator.add]
    # The 'next' field indicates where to route to next
    next: str

#endregion


#region agents and nodes creation functions

def create_agent(
    llm: ChatOpenAI,
    tools: list,
    system_prompt: str
) -> AgentExecutor:
    # Each worker node will be given a name and some tools.
    prompt = ChatPromptTemplate.from_messages(
        [
            (
                'system',
                system_prompt,
            ),
            MessagesPlaceholder(variable_name='messages'),
            MessagesPlaceholder(variable_name='agent_scratchpad'),
        ]
    )
    agent = create_openai_tools_agent(llm, tools, prompt)
    executor = AgentExecutor(agent=agent, tools=tools)
    return executor

def agent_node(state, agent, name):
    result = agent.invoke(state)
    return {'messages': [HumanMessage(content=result['output'], name=name)]}

def create_and_add_node(
    agent: AgentExecutor,
    name: str,
    workflow: StateGraph
):
    node = functools.partial(
        agent_node,
        agent = agent,
        name = name
    )
    workflow.add_node(name, node)

#endregion


#region setup functions

def setup_supervisor(llm: ChatOpenAI, workflow: StateGraph, members: [str]):
    system_prompt = (
        'You are a supervisor tasked with managing a conversation between the'
        ' following workers: {members}. Given the following user request,'
        ' respond with the worker to act next. Each worker will perform a'
        ' task and respond with their results and status. When finished,'
        ' respond with FINISH.'
    )
    # Our team supervisor is an LLM node. It just picks the next agent to process
    # and decides when the work is completed
    options = ['FINISH'] + members
    # Using openai function calling can make output parsing easier for us
    function_def = {
        'name': 'route',
        'description': 'Select the next role.',
        'parameters': {
            'title': 'routeSchema',
            'type': 'object',
            'properties': {
                'next': {
                    'title': 'Next',
                    'anyOf': [
                        {'enum': options},
                    ],
                }
            },
            'required': ['next'],
        },
    }
    prompt = ChatPromptTemplate.from_messages(
        [
            ('system', system_prompt),
            MessagesPlaceholder(variable_name='messages'),
            (
                'system',
                'Given the conversation above, who should act next?'
                ' Or should we FINISH? Select one of: {options}',
            ),
        ]
    ).partial(options=str(options), members=', '.join(members))

    supervisor_chain = (
        prompt
        | llm.bind_functions(functions=[function_def], function_call='route')
        | JsonOutputFunctionsParser()
    )
    workflow.add_node('supervisor', supervisor_chain)

    workflow.set_entry_point('supervisor')


def setup_edges(workflow: StateGraph, members: [str]):
    for member in members:
        # We want our workers to ALWAYS 'report back' to the supervisor when done
        workflow.add_edge(member, 'supervisor') # add one edge for each of the agents

    # The supervisor populates the 'next' field in the graph state
    # which routes to a node or finishes
    conditional_map = {k: k for k in members}
    conditional_map['FINISH'] = END
    workflow.add_conditional_edges('supervisor', lambda x: x['next'], conditional_map)

#endregion


#region lifecycle functions

def load() -> StateGraph:
    workflow = StateGraph(AgentState)
    return workflow


def compile_graph(workflow: StateGraph) -> Pregel:
    return workflow.compile()


def stream(graph: Pregel, start_message: str):
    for s in graph.stream(
        {
            'messages': [
                HumanMessage(content = start_message)
            ]
        }
    ):
        if '__end__' not in s:
            print(s)
            print('----')


def invoke(graph: Pregel, start_message: str) -> str:
    final_response = graph.invoke(
        {
            'messages': [
                HumanMessage(content = start_message)
            ]
        }
    )
    return final_response['messages'][1].content

#endregion
