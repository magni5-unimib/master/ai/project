'''
The main script. Where all the magic begins.
'''


#region imports

# basic
import sys
import time

# mine
import graph
import lg
import lm
import netlogo
import satnav
from travelers import Traveler, TravelerAgent
import world

#endregion


#region config

TRAVELERS_CARDINALITY = {
    'agent': 0,
    'total': 10
}
TARGET_VERTEX = 55
MAX_TICKS = 80
CLOSE_AT_END = False

#endregion


#region setup

def setup_vertices():
    # leave those commented lines as example to create an empty agentset and add
    # elements iteratively
    # world.NETLOGO.command(f'set vertices no-patches')
    for v in world.G.nodes:
        # world.NETLOGO.command(f'''
        #     set vertices (patch-set vertices patch {world.G.v_x(v)} {world.G.v_y(v)} [
        #         set vertex-id {v}
        #     ])
        # ''')
        world.NETLOGO.command(f'''
            ask patch {world.G.v_x(v)} {world.G.v_y(v)} [
                set vertex-id {v}
                ; style
                set pcolor 5
                set plabel "{v}({world.G.v_x(v)},{world.G.v_y(v)})"
                set plabel-color black
            ]
        ''')
    world.NETLOGO.command('set vertices patches with [ vertex-id != -1 ]')
    world.NETLOGO.command(f'''
        ask patch {world.G.v_x(TARGET_VERTEX)} {world.G.v_x(TARGET_VERTEX)} [
            set pcolor orange 
        ]
    ''')

def setup_edges():
    for s, t in world.G.edges:
        world.NETLOGO.command(f'''
            ask patch {world.G.e_x(s, t)} {world.G.e_y(s, t)} [
                set edge-source {s}
                set edge-target {t}
                set pcolor 9
            ]
        ''')
    world.NETLOGO.command('set edges patches with [ edge-source != -1 ]')

def create_traveler(who: int, is_agent: bool = False):
    world.NETLOGO.command(f'''
        create-travelers 1 [
            move-to one-of vertices with [ not any? travelers-on self ]
            ; attributes
            set source-xcor xcor
            set source-ycor ycor
            set target-xcor {world.G.v_x(TARGET_VERTEX)}
            set target-ycor {world.G.v_y(TARGET_VERTEX)}
            set is-traveler-agent? {is_agent}
            set passenger-of -1
            set has-passenger -1
            ;set next-patch -1
            ; style
            set color (ifelse-value is-traveler-agent? [green] [red])
            set label who
        ]
    ''')

    # since travelers are created only at the beginning 'who' will always
    #   be equal to the index of the loop
    # who = world.NETLOGO.report('[who] of last sort-by < travelers')

    # this would be okay if I would not have any other parameters to specify
    # TypeOfNewTraveler = TravelerAgent if i == 0 else Traveler
    # new_traveler = TypeOfNewTraveler(world.SATNAV, world.NETLOGO, who)
    if is_agent:
        new_traveler = TravelerAgent(
            world.LLM,
            world.AGENT_WORKFLOW,
            world.SATNAV,
            world.NETLOGO,
            who
        )
    else:
        new_traveler = Traveler(
            world.SATNAV,
            world.NETLOGO,
            who
        )
    new_traveler.target = TARGET_VERTEX
    source = new_traveler.get_attribute('[ vertex-id ] of patch-here')
    new_traveler.source = source
    world.TRAVELERS.append(new_traveler)

def setup_travelers():
    i = 0
    while i < TRAVELERS_CARDINALITY.get('agent'):
        create_traveler(i, True)
        i += 1
    while i < TRAVELERS_CARDINALITY.get('total'):
        create_traveler(i, False)
        i += 1

def setup_langgraph():
    satnav_agent = lg.create_agent(
        world.LLM,
        [satnav.calculate_path],
        '''
        You are the manager of the SatNav.
        You will be asked to calculate the path between two vertices and
        you will return the result.
        '''
    )
    lg.create_and_add_node(satnav_agent, 'SatNavManager', world.AGENT_WORKFLOW)

    # netlogo_agent = lg.create_agent(
    #     world.LLM,
    #     [tools.get_netlogo_attribute, tools.set_netlogo_attribute],
    #     '''
    #     You are the manager of the NetLogo environment.
    #     You will be asked to gets or sets attributes and you will return the result.
    #     '''
    # )
    # lg.create_and_add_node(netlogo_agent, 'NetLogoManager', world.AGENT_WORKFLOW)

    members = ['SatNavManager'] #, 'NetLogoManager']
    for t in world.TRAVELERS:
        if type(t) is TravelerAgent:
            members.append(t.name)

    lg.setup_supervisor(world.LLM, world.AGENT_WORKFLOW, members)

    lg.setup_edges(world.AGENT_WORKFLOW, members)

    world.AGENT_GRAPH = lg.compile_graph(world.AGENT_WORKFLOW)

def setup():
    world.G = graph.load()

    world.SATNAV = satnav.SatNav()
    world.SATNAV.set_graph(world.G)

    world.LLM = lm.load()

    world.NETLOGO = netlogo.load()

    world.AGENT_WORKFLOW = lg.load()

    setup_vertices()
    setup_edges()
    setup_travelers()

    # this is placed after setup_travelers() because all of the agents must be
    # added to the graph before compiling it
    # it can be removed if each agent compile the workflow by itself
    setup_langgraph()

#endregion


#region execute

def tick_travelers():
    for t in world.TRAVELERS:
        if t.alive:
            t.tick()
        else:
            world.TRAVELERS.remove(t)

def execute():
    # sleep to
    time.sleep(5)
    world.NETLOGO.output('------------ START ------------', 1, 2)

    i = 0
    while i < MAX_TICKS and world.TRAVELERS:
        world.NETLOGO.output(f'------------ TICK {i} ------------', 2, 1)
        tick_travelers()
        world.NETLOGO.command('tick')
        i += 1

    world.NETLOGO.output(f'------------ END (TICK {i - 1}) ------------', 3, 1)

#endregion


#region end

def end():
    if CLOSE_AT_END:
        world.NETLOGO.kill_workspace()

#endregion


#region main

def main():
    setup()
    execute()
    end()

if __name__ == '__main__':
    main()
    sys.exit(0)

#endregion
