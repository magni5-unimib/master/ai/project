# Artificial intelligence

## Project proposal

### Team

Marco Magni 851810

### Title

LLM-based multi-agent system in NetLogo

### Abstract

Transportation efficiency is a crucial issue for improving the well being of citizens. A starting point in developing opinions and solutions could be modeling and simulating a realistic scenario.

The scenario considered for this project consists in many people who have to organize themselves in order to reach a common destination, trying to minimize the costs in terms of total distance covered.

Modeling of the problem will be done by implementing a multi-agent system in which agents communicate via large language model, as the people they represent would do in real life.

Starting from two of the main surveys on the topic [1, 2], possible approaches for the MAS architecture will be explored and considered, with a special attention to the one proposed by researchers at Stanford and Google. [3]

### Tools

The software stack that will be used includes:
- *Python* as programming language
- *NetLogo* as visual interface
- *pynetlogo* library that allows Python to interact with NetLogo and to execute commands. [4]

In addition, *LangChain* framework will be used to support and orchestrate the language model infrastructure.

Two issues about LLMs are choosing which model to use and choosing between running it locally or accessing via APIs.
My idea is to begin working locally (setupping them with *Ollama*) and trying various models (llama2, mistral 7B, phi-2, tinyllama, ...). If the results will not be satisfying and coherent with the goal of the project, then I will consider using APIs to access to heavier models.

### Papers

1. Wang, Lei, et al. "A survey on large language model based autonomous agents." arXiv preprint arXiv:2308.11432 (2023).
2. Xi, Zhiheng, et al. "The rise and potential of large language model based agents: A survey." arXiv preprint arXiv:2309.07864 (2023).
3. Park, Joon Sung, et al. "Generative agents: Interactive simulacra of human behavior." Proceedings of the 36th Annual ACM Symposium on User Interface Software and Technology. 2023.
4. Jaxa-Rozen, Marc, and Jan H. Kwakkel. "Pynetlogo: Linking netlogo with python." Journal of Artificial Societies and Social Simulation 21.2 (2018).

Those are the starting references. Others will certainly be explored and considered during the implementation of the project.
